import argparse
import pandas as pd
import sqlite3
import datetime
from bs4 import BeautifulSoup

sql_create_pages_table = """
  CREATE TABLE IF NOT EXISTS Pages (
    ID integer PRIMARY KEY AUTOINCREMENT NOT NULL,
    Url text NOT NULL,
    Title text NOT NULL,
    Content text NOT NULL,
    Timestamp DATETIME NOT NULL
  );"""

sql_insert_pages = """
  INSERT INTO pages(Url, Title, Content, Timestamp)
    VALUES(?,?,?,?)"""

def blocks(iterable):
    accumulator= []

    for line in iterable:
        if "</ID>" in line:
            accumulator.append(line)
            yield accumulator
            accumulator = []
        elif line:
            accumulator.append(line)

def date_parser(t):
    return datetime.datetime.fromtimestamp(int(t))


def write_biz(src, conn):
    result = pd.read_table(src, header=None, usecols=[0,2,3,4,5,6],
        parse_dates=[5], date_parser=date_parser, names=["Biz ID", "Biz Name", "Biz Code",
                                                         "Biz Description", "QRCode", "Timestamp"])

    result.to_sql("Biz", conn, flavor="sqlite", index_label="ID")


def write_clicks(src, conn):
    result = pd.read_table(src, header=None, usecols=[1, 2, 3, 4, 5], parse_dates=[4],
        date_parser=date_parser, names=["URL", "Title", "Read Number", "Like Number", "Timestamp"])

    result.to_sql("Clicks", conn, flavor="sqlite", index_label="ID")


def write_pages(src, conn):
    conn.execute(sql_create_pages_table)
    with open(src, encoding='utf-8') as f:
        for b in blocks(f):
            bs = BeautifulSoup("".join(b), "html.parser")

            timestamp = datetime.datetime.fromtimestamp(int(bs.time.text))
            url = bs.url.text
            title = bs.title.text
            body = bs.body.prettify()

            conn.execute(sql_insert_pages, (url, title, body, timestamp))
    conn.commit()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--biz', type=str, default="../wechat_test/biz")
    parser.add_argument('--clicks', type=str, default="../wechat_test/clicks")
    parser.add_argument('--pages', type=str, default="../wechat_test/pages")

    args = parser.parse_args()
    biz = args.biz
    clicks = args.clicks
    pages = args.pages

    conn = sqlite3.connect("data.dt")

    write_biz(biz, conn)
    write_clicks(clicks, conn)
    write_pages(pages, conn)

    conn.close()