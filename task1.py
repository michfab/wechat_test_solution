import argparse
import pandas as pd


def write_biz(src, dst):
    result = pd.read_table(src, header=None, usecols=[0, 2, 4],
                           names=["Biz ID", "Biz name", "Biz description"], index_col=0)
    result.to_csv(dst, encoding='utf-8', sep="\t")


def write_clicks(src, dst):
    result = pd.read_table(src, header=None, usecols=[3, 4],
                           names=["Max read count per article", "Max like count per article"],
                           index_col=0)
    result.to_csv(dst, encoding='utf-8', sep="\t")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--biz', type=str, default="../wechat_test/biz")
    parser.add_argument('--clicks', type=str, default="../wechat_test/clicks")
    parser.add_argument('--pages', type=str, default="../wechat_test/pages")

    args = parser.parse_args()
    biz = args.biz
    clicks = args.clicks
    pages = args.pages

    write_biz(biz, "%s.csv" % biz)
    write_clicks(clicks, "%s.csv" % clicks)






